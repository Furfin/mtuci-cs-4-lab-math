import numpy as np
import matplotlib.pyplot as plt
import math

from numpy.core.fromnumeric import shape

n=1
arr = np.zeros((9,9))
for i in range(0,9,3):
    for j in range(0,9,3):
        arr[i:i+3,j:j+3] = n
        n+=1




a = np.array([[1,6],[2,8],[3,11],[3,10],[1,7]])
a_mean = [a[0:6,i].mean() for i in range(2)]



print(arr,a,a_mean,sep = '\n\n')

fig,ax = plt.subplots()
x = np.linspace(10,-10,10000)

ax.plot(x,[math.sin(i) for i in x],color = 'g')
ax.plot(x,[math.cos(i) for i in x],color = 'r')
ax.plot(x,[math.cos(i) + math.sin(i) for i in x],color = 'y')
ax.plot(x,[ math.sin(i) - math.cos(i) for i in x],color = 'b')

fig2,(ax1,ax2,ax3,ax4) = plt.subplots(1,4,sharey=True)

ax1.plot(x,[math.sin(i) for i in x],color = 'g')
ax2.plot(x,[math.cos(i) for i in x],color = 'r')
ax3.plot(x,[math.cos(i) + math.sin(i) for i in x],color = 'y')
ax4.plot(x,[ math.sin(i) - math.cos(i) for i in x],color = 'b')
plt.show()